package org.example;

public class UserService {

     private UserDb userDb = new UserDb();
     static final int MIN_LENGTH=8;

    public boolean changePassword(String username , String password)throws Exception
    {
        if (password.length() >=MIN_LENGTH)
            return userDb.changePassword(username,password);

        throw new Exception("Weak password");
    }
}
