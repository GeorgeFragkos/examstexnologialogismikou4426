package org.example;
import org.junit.Test;
import org.mockito.Mockito;


import static org.junit.Assert.*;


public class UserServiceTest {

    UserDb userDb = Mockito.mock(UserDb.class);

    UserService userService = new UserService();

    @Test
    public void TestWhenPasswordChangeSuccessful()throws Exception
    {
        Mockito.when(userDb.changePassword("GeorgeF","12345678@#4")).thenReturn(true);
        boolean actual = userService.changePassword("GeorgeF","12345678@#4");
        assertFalse(actual);
    }

    @Test(expected = Exception.class)
    public void TestWhenPasswordDoestFollowTheRules()throws Exception
    {
        assertFalse(userService.changePassword("GeorgeF","123"));
    }

    @Test
    public void TestWhenUsernameDoestExists()throws Exception
    {

        Mockito.when(userDb.changePassword("Geor","12345678@#4")).thenReturn(false);
        boolean actual = userService.changePassword("Geor","12345678@#4");
        assertFalse(actual);
    }

}